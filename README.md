# Spring Boot OAuth2 Authorization Server JWT with MySQL Database

### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-oauth2-authorization-server-jwt-mysql.git`.
2. Go inside the folder: `cd springboot-oauth2-authorization-server-jwt-mysql`.
3. Run application: `gradle clean bootRun`
4. Open your favorite terminal:
```shell script
curl --location --request POST 'http://localhost:8080/oauth/token ' \
--header 'Authorization: Basic b2F1dGgyLWp3dC1jbGllbnQ6YWRtaW4xMjM0' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=admin' \
--data-urlencode 'password=admin1234' \
--data-urlencode 'grant_type=client_credentials'
```

Or Use case use Postman

## Screen shot

Header Input

![Header Input](img/auth.png "Header Input")

Body Input

![Body Input](img/form.png "Body Input")