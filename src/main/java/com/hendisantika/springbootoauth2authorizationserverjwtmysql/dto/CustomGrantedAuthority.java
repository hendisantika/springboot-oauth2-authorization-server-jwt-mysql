package com.hendisantika.springbootoauth2authorizationserverjwtmysql.dto;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-oauth2-authorization-server-jwt-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/04/20
 * Time: 08.25
 */
public class CustomGrantedAuthority implements GrantedAuthority, Serializable {

    private final String name;

    public CustomGrantedAuthority(String name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }
}
