package com.hendisantika.springbootoauth2authorizationserverjwtmysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootOauth2AuthorizationServerJwtMysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootOauth2AuthorizationServerJwtMysqlApplication.class, args);
    }

}
