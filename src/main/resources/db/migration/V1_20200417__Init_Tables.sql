-- ----------------------------
-- Table structure for authority
-- ----------------------------
CREATE TABLE authority
(
    id   bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    name varchar(20) DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY AUTHORITY_UNIQUE_NAME (NAME)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------
CREATE TABLE oauth_access_token
(
    id                bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    authentication    mediumblob   DEFAULT NULL,
    authentication_id varchar(255) DEFAULT NULL,
    client_id         varchar(255) DEFAULT NULL,
    refresh_token     varchar(255) DEFAULT NULL,
    token             mediumblob   DEFAULT NULL,
    token_id          varchar(255) DEFAULT NULL,
    user_name         varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
CREATE TABLE oauth_client_details
(
    id                      bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    access_token_validity   int(11) DEFAULT NULL,
    additional_information  varchar(4096) DEFAULT NULL,
    authorities             varchar(255)  DEFAULT NULL,
    authorized_grant_types  varchar(255)  DEFAULT NULL,
    autoapprove             tinyint(4)    DEFAULT NULL,
    client_id               varchar(255)  DEFAULT NULL,
    client_name             varchar(255)  DEFAULT NULL,
    client_secret           varchar(255)  DEFAULT NULL,
    created                 datetime(6)   DEFAULT NULL,
    enabled                 tinyint(1)    DEFAULT 1,
    refresh_token_validity  int(11) DEFAULT NULL,
    resource_ids            varchar(255)  DEFAULT NULL,
    scope                   varchar(255)  DEFAULT NULL,
    uuid                    varchar(255)  DEFAULT NULL,
    web_server_redirect_uri varchar(255)  DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------
CREATE TABLE oauth_client_token
(
    id                bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    authentication_id varchar(255) DEFAULT NULL,
    client_id         varchar(255) DEFAULT NULL,
    token             mediumblob   DEFAULT NULL,
    token_id          varchar(255) DEFAULT NULL,
    user_name         varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
CREATE TABLE oauth_code
(
    id             bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    authentication mediumblob   DEFAULT NULL,
    code           varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------
CREATE TABLE oauth_refresh_token
(
    id             bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    authentication mediumblob   DEFAULT NULL,
    token          mediumblob   DEFAULT NULL,
    token_id       varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE TABLE user
(
    id                  bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    account_expired     bit(1)       DEFAULT NULL,
    account_locked      bit(1)       DEFAULT NULL,
    credentials_expired bit(1)       DEFAULT NULL,
    enabled             bit(1)       DEFAULT NULL,
    password            varchar(255) DEFAULT NULL,
    user_name           varchar(50)  DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY USER_UNIQUE_USERNAME (user_name)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_authority
-- ----------------------------
CREATE TABLE user_authority
(
    id           bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    authority_id bigint(20) unsigned DEFAULT NULL,
    user_id      bigint(20) unsigned DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY USER_AUTHORITY_UNIQUE_USER_ID_AND_AUTHORITY_ID (user_id,authority_id),
    KEY          FK_USER_AUTHORITY_AUTHORITY_ID(authority_id),
    CONSTRAINT FK_USER_AUTHORITY_AUTHORITY_ID FOREIGN KEY (authority_id) REFERENCES authority (id),
    CONSTRAINT FK_USER_AUTHORITY_USER_ID FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
